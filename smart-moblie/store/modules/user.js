

import Vue from 'vue';

const user = {
	state: {
		user:{}
	},
	
	actions: {
		getLoginUser({
			commit,
			state
		}) { //获取用户基础信息(通过openid获取)
			return new Promise((resolve, reject) => {
				let auth = uni.getStorageSync('authentication');
				Vue.prototype.$http.getUserDetail().then(res=>{
					if(res.code==0){
						if(!res.user){
							 uni.clearStorageSync('authentication');
							 Vue.prototype.$tools.wechatAuthorize();
							
							 return;
						}
						commit('SET_USER', res.user)
						resolve(res)
					}else{
						Vue.prototype.$tools.wechatAuthorize();
					}
				}).catch((error) => {
					 Vue.prototype.$tools.wechatAuthorize();
				})
			})
		},
	},
	mutations: {
		SET_USER: (state, userInfo) => {
			state.userInfo = userInfo
		},
	}
}
export default user
