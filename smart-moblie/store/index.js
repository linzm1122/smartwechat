import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user.js'
import getters from './getters'


Vue.use(Vuex)
const store = new Vuex.Store({
	modules: {user},
	getters: getters,
	
	mutations: {
		setLoginCode(state, loginCode){
			state.loginCode = loginCode;
		},
		setPrefixImg(state, url) {
            state.prefixImg = url;
        }
	
	},
})

export default store