
import Vue from 'vue';

function msg(title, duration = 3000, mask = true, icon = "none") {
	//统一提示方便全局修改
	if (Boolean(title) === false) {
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon,
	});
};
function deviceTypeList(){
	return [	{
					id:1,name1:'腕表',name:'智能腕表',content:'24小时记录身体状态',icon:"9",enName:"swatch",
				},
				{
					id:2,name1:'床垫',name:'睡眠监测床垫',content:'采集心率 呼吸率数据',icon:"8",enName:"bed",
				}
			]
}

export function mymap(ak) {
	return new Promise(function(resolve, reject) {
		window.init = function() {
			resolve(mymap)
		}
		var script = document.createElement('script')
		script.type = 'text/javascript'
		script.src =
			`http://api.map.baidu.com/api?v=1.0&type=webgl&ak=${ak}&mcode=35:81:1C:D3:2A:63:FB:6B:6E:1C:D4:F7:81:DF:A5:1B:89:57:83:91;uni.UNI342DC80&callback=init`
		script.onerror = reject
		document.head.appendChild(script)
	})
}

/**
 * 返回上一页
 * @param {*} noPageUrl 没有上一页跳转路径
 */

function backPrePage(noPageUrl) {
	//返回上一页 。没有上一页返回首页
	let pages = getCurrentPages();
	let prevPage = pages[pages.length - 2];
	 if (prevPage) { //如果有上一页 返回
		uni.navigateBack();
	} else { //没有上一页 ，没有设置 noPageUrl
		if (noPageUrl) { //自定义返回页
			uni.navigateTo({
				url: noPageUrl,
				success: res => {},
				fail: () => {},
				complete: () => {}
			});
		}else{
			uni.navigateTo({
				url: '/pages/index/index',
				success: res => {},
				fail: () => {},
				complete: () => {}
			});
			
		}
		
	}


};


function loginInfo(openid){
	return new Promise((resolve, reject) => {
		let token = uni.getStorageSync('authentication');
		if(openid){
			Vue.prototype.$http.wechatLogin({openid:openid}).then(res=>{
					if(res.code ==0){
						uni.setStorageSync('authentication',res.token);
						Vue.prototype.$store.dispatch("getLoginUser").then(res=>{
							console.log(111111111111111);
							resolve(res);
							
						})
					}
			})
		}else if(token){
			Vue.prototype.$store.dispatch("getLoginUser").then(res=>{
				resolve(res)
			});
		}else{
			resolve(res)
		}
		
		
		
	})
	
	
	
	
}


function wechatAuthorize()
	{
	    var pages = getCurrentPages() // 获取栈实例
	    	let currentRoute = pages[pages.length - 1].route; // 获取当前页面路由
	    	let currentPage = pages[pages.length - 1]['$page']['fullPath'] //当前页面路径(带参数
	    	console.log(currentPage)
	    
	    if(currentPage.indexOf("openid")<0){
	    	location.replace(Vue.prototype.$http.wechatAuthorize())
	    }
	    
	}
/**
 * 验证身份证
 * @param {*} card 身份证号码
 * @returns 
 */
function checkParity(card) {
	if (!card) { //身份证为空
		return false;
	}
	//15位转18位
	card = changeFivteenToEighteen(card);
	var len = card.length;
	if (len == '18') {
		var arrInt = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
		var arrCh = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
		var cardTemp = 0,
			i, valnum;
		for (i = 0; i < 17; i++) {
			cardTemp += card.substr(i, 1) * arrInt[i];
		}
		valnum = arrCh[cardTemp % 11];
		if (valnum == card.substr(17, 1)) {
			return true;
		}
		return false;
	}
	return false;
};
/**
 * 15位转18位
 * @param {*} card 身份证号码
 * @returns 
 */
function changeFivteenToEighteen(card) {
	if (card.length == '15') {
		var arrInt = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
		var arrCh = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
		var cardTemp = 0,i;
		card = card.substr(0, 6) + '19' + card.substr(6, card.length - 6);
		for (i = 0; i < 17; i++) {
			cardTemp += card.substr(i, 1) * arrInt[i];
		}
		card += arrCh[cardTemp % 11];
		return card;
	}
	return card;
};
function validatePhoneNumber(phoneNumber) {
	const regex = /^1[3|4|5|6|7|8|9][0-9]\d{8}$/; // 手机号正则表达式
	return regex.test(phoneNumber);
  }
  




//增零

module.exports = {
	validatePhoneNumber,
	checkParity,//验证身份证
	wechatAuthorize,//跳转
	loginInfo,//登录
	backPrePage,//返回上一页
	mymap,//百度地图
	msg, //重新封装的uni.showToast
	deviceTypeList,//设备类型
};
