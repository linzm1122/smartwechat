
import Vue from 'vue';
let msg=[
	{
		en:'user_is_not_exit',
		cn:'登录用户信息不存在'
	},
	{
		en:'cardId_is_exist',
		cn:'身份证信息已经存在'
	},
	{
		en:'device_is_binding_other',
		cn:'设备已经绑定其他人'
	},
	{
		en:'device_is_binding_myself',
		cn:'重复绑定'
	},
	{
		en:'customer_info_is_not_exist',
		cn:'用户不存在'
	},
	{
		en:'device_is_not_exist',
		cn:'设备不存在'
	},
	{
		en:'not_finish_person_authen',
		cn:'未完成个人认证'
	},
	{
		en:'alarm_data_is_not_exist',
		cn:'报警信息不存在'
	},
	{
		en:'alarm_is_clear',
		cn:'报警信息已经清除'
	},
]


function getMsgCN(en){
	for(let i in msg){
		if(en==msg[i].en){
			return msg[i].cn
		}
	}
	return en;
}




//增零

module.exports = {
	getMsgCN
};
