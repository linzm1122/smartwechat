import http from '../store/index';
import Vue from 'vue';
import config from "@/config.js"
module.exports = {
	config: function(name) {
		return config.APIHOST;
	},
	post: function(url, data, xAuthToken = true) {
		
		url = this.config("APIHOST")+url;
		let header = {
			// "content-type": "application/x-www-form-urlencoded"
		}
		if (xAuthToken) {
			let token = uni.getStorageSync('authentication');
			header = {
				// "content-type": "application/x-www-form-urlencoded",
				"token": token
			}
		}
		return new Promise((succ, error) => {
			uni.request({
				url: url,
				data: data,
				method: "POST",
				header: header,
				success: function(result) {
					succ.call(self, result.data)
				},
				fail: function(e) {
					// location.replace(Vue.prototype.$http.wechatAuthorize())
					error.call(self, e)
				}
			})
		})
	},
	urlFormDataPost: function(url, data, xAuthToken = true) {
		url = this.config("APIHOST")+url;
		let header = {
			// "content-type": "application/x-www-form-urlencoded"
		}
		if (xAuthToken) {
			let token = uni.getStorageSync('authentication');
			header = {
				// "content-type": "application/x-www-form-urlencoded",
				"token": token
			
			}
		}
		return new Promise((succ, error) => {
			uni.request({
				url: url,
				data: data,
				method: "POST",
				header: header,
				success: function(result) {
					let res=result.data
					if(res.code===200){
						succ.call(self, result.data)
					}else if(res.code===500){
						uni.showToast({
							title: "系统异常，请稍后再试",
							icon:"none",
							duration: 2000
						});
						error.call(self,res.msg)
					}else{
						uni.showToast({
							title: res.msg,
							icon:"none",
							duration: 2000
						});
						error.call(self,res.msg)
					}
				},
				fail: function(e) {
					error.call(self, e)
				}
			})
		})
	},
	get: function(url, data, xAuthToken = true) {
		url = this.config("APIHOST")+url;
		let header = {
			"content-type": "application/x-www-form-urlencoded"
		}
		if (xAuthToken) {
			let token = uni.getStorageSync('authentication');
			header = {
				"content-type": "application/x-www-form-urlencoded",
				"token": token
			}
		}
		return new Promise((succ, error) => {
			uni.request({
				url: url,
				data: data,
				method: "GET",
				header: header,
				success: function(result) {
					succ.call(self, result.data)
				},
				fail: function(e) {
					// location.replace(Vue.prototype.$http.wechatAuthorize())
					error.call(self, e)
				}
			})
		})
	}

}
