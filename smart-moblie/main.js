import Vue from 'vue';
import App from './App';

import store from './store'

Vue.config.productionTip = false;

Vue.prototype.$store = store;

App.mpType = 'app';

import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

tools.js

import tools from '@/utils/tools.js'
Vue.prototype.$tools= tools


import msg from '@/utils/msg.js'
Vue.prototype.$msg= msg


import http from '@/common/http.api.js'
Vue.prototype.$http= http

import '@/utils/router.js';
import tabbar from "@/components/tabbar"
Vue.component('tabbar', tabbar)

const app = new Vue({
	store,
	...App
});



app.$mount();
