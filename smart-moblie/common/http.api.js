// import { data } from 'jquery'

import config from "@/config.js"
import httpRequest from '../utils/http'

module.exports = {
	prefixImg: function() { //获取资源前缀
		return httpRequest.get('/image/prefix', {})
	},
	getUserDetail:function(){//获取登录人详情
		return httpRequest.get('/wechat/userDetail', {})
	},
	wechatAuthorize:function(data){//授权链接
		return config.APIHOST+'/wechat/authorize';
	},
	wechatLogin:function(data){//根据openid 获取
		return httpRequest.get('/wechat/login', data)
	},
	addCustomerInfo:function(data){//添加亲人
		return httpRequest.post('/wechat/addCustomerInfo', data)
	},
	addlnfoDevice:function(data){//添加设备
		return httpRequest.post('/wechat/addInfoDevice', data)
	},
	alarmList:function(data){//获取警报信息
		return httpRequest.get('/wechat/alarmList', data)
	},
	alarmDetail:function(data){//获取警报详情
		return httpRequest.get('/wechat/alarmDetail', data)
	},
	clearAlarm:function(data){//取消警报详情
		return httpRequest.get('/wechat/clearAlarm', data)
	},
	
	myRelation:function(data){//我的关注
		return httpRequest.get('/wechat/myRelation', data)
	},
	cancelRelation:function(data){//取消关注
		return httpRequest.get('/wechat/cancelRelation', data)
	},
	findCustomerInfoForCardId:function(data){//取消关注
		return httpRequest.get('/wechat/findCustomerInfoForCardId', data)
	},
	
	deviceDetail:function(data){//设备详情
		return httpRequest.get('/wechat/deviceDetail', data)
	},
	cancelBindDevice:function(data){//设备接触绑定
		return httpRequest.get('/wechat/cancelBindDevice', data)
	},
	deviceList:function(data){//关注人的设备列表
		return httpRequest.get('/wechat/infoDetail', data)
	},
	addMyAuthen:function(data){//认证
		return httpRequest.post('/wechat/addMyAuthen', data)
	},
	deviceFeedback:function(data){//异常反馈
		return httpRequest.post('/wechat/deviceFeedback', data)
	},
	healthList:function(data){//我的健康信息
		return httpRequest.get('/wechat/healthList', data)
	},
	locationList:function(data){//轨迹信息
		return httpRequest.get('/wechat/locationList', data)
	},
	deviceFence:function(data){//电子围栏
		return httpRequest.post('/wechat/deviceFence', data)
	},
	deviceFenceDetail:function(data){//电子围栏详情
		return httpRequest.get('/wechat/deviceFenceDetail', data)
	},

	

}
